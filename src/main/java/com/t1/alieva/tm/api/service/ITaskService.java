package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    Task create(String userId,String name) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task create(String userId,String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    List<Task> findAllByProjectId(String userId,String projectId) throws AbstractFieldException;

    Task updateById(String userId,String id, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task updateByIndex(String userId, Integer index, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task changeTaskStatusByIndex(String userId, Integer index, Status status) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task changeTaskStatusById(String userId,String id, Status status) throws AbstractFieldException;



}
