package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.api.repository.IRepository;
import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.model.AbstractModel;

import java.util.List;

public interface IService <M extends AbstractModel> extends IRepository<M>
{

}
