package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.api.repository.IUserOwnedRepository;
import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService <M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>
{
}
