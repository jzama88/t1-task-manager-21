package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.IdEmptyException;
import com.t1.alieva.tm.exception.field.IndexIncorrectException;
import com.t1.alieva.tm.exception.field.UserIdEmptyException;
import com.t1.alieva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository <M extends AbstractUserOwnedModel> extends IRepository<M>
{
    void clear(String userId) throws UserIdEmptyException, AbstractFieldException;

    boolean existsById(String userId, String id) throws IdEmptyException, AbstractFieldException;

    List<M> findAll(String userId) throws UserIdEmptyException, AbstractFieldException;

    List<M> findAll(String userId, Comparator comparator) throws AbstractFieldException;

    M findOneById(String userId, String id) throws IdEmptyException, AbstractFieldException;

    M findOneByIndex(String userId, Integer index) throws AbstractFieldException;

    int getSize(String userId) throws AbstractFieldException;

    M removeById(String userId, String id) throws IdEmptyException, AbstractFieldException;

    M removeByIndex(String userId, Integer index) throws IndexIncorrectException, AbstractFieldException;

    M add(final String userId, M model) throws AbstractFieldException;

    M remove(final String userId, M model) throws AbstractFieldException;

}

