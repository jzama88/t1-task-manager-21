package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    Task create(String userId,String name) throws AbstractEntityNotFoundException;

    Task create(String userId,String name,String description) throws AbstractEntityNotFoundException;

    List<Task> findAllByProjectId(String userId, String projectId);

}
