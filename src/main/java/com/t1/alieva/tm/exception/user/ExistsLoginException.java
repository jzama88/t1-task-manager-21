package com.t1.alieva.tm.exception.user;

public final class ExistsLoginException extends AbstractUserException{

    public ExistsLoginException() {
        super("Error! Login already exists...");
    }
}
