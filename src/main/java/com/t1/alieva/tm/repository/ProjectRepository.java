package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.IProjectRepository;
import com.t1.alieva.tm.exception.entity.ProjectNotFoundException;
import com.t1.alieva.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {


    @Override
    public Project create(final String userId,final String name,final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public Project create(final String userId, final String name) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }
}
