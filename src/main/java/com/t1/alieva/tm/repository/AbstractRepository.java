package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.IRepository;
import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository <M extends AbstractModel> implements IRepository<M>
{
    protected final List<M> models = new ArrayList<>();

    @Override
    public M add(M model) {
        models.add(model);
        return model;
    }

    @Override
    public List<M> findAll(String userId, Sort sort) {
        return models;
    }

    @Override
    public List<M> findAll(Comparator<M> comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(String id) {
        return models
                .stream()
                .filter(r -> id.equals(r.getId()))
                .findFirst().orElse(null);

    }

    @Override
    public M findOneByIndex(Integer index) {
        return models.get(index);
    }

    @Override
    public M remove(M model) {
       models.remove(model);
       return model;
    }

    @Override
    public M removeById(String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public M removeByIndex(String userId, Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public void clear()
    {
        models.clear();
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public boolean existsById(String id) {
        return findOneById(id) != null;
    }
}
