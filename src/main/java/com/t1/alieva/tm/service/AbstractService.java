package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.IRepository;
import com.t1.alieva.tm.api.repository.IService;
import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.ModelNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.IdEmptyException;
import com.t1.alieva.tm.exception.field.IndexIncorrectException;
import com.t1.alieva.tm.exception.field.UserIdEmptyException;
import com.t1.alieva.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService <M extends AbstractModel, R extends IRepository<M>> implements IService<M>
{
    protected final R repository;

    public AbstractService(R repository) {
        this.repository = repository;
    }

    @Override
    public M add(M model) throws AbstractEntityNotFoundException {
        if(model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @Override
    public List<M> findAll(String userId, Sort sort) throws UserIdEmptyException, AbstractFieldException {
        return repository.findAll(userId, sort);
    }

    @Override
    public List<M> findAll(Comparator<M> comparator) {
        if(comparator == null) return findAll(comparator);
        return repository.findAll(comparator);
    }

    @Override
   /* public List<M> findAll(Sort sort) {
        if (sort == null) return findAll(userId, sort);
        return findAll(sort.getComparator());

    }*/


    public M findOneById(String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);

    }

    @Override
    public M findOneByIndex(Integer index) throws AbstractFieldException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);

    }

    @Override
    public M remove(M model) throws AbstractEntityNotFoundException {
        if (model == null) return null;
        return repository.remove(model);
    }

    @Override
    public M removeById(String id) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);

    }

    @Override
    public M removeByIndex(String userId, Integer index) throws AbstractFieldException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);

    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }
}
